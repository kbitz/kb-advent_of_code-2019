input_6 = [line.rstrip() for line in open('6_input.txt')]

orbits = [tuple(map(str, orbit.split(')'))) for orbit in input_6]

def find_direct_orbit(obj,orbits):
  direct_orbit = [x for x, val in enumerate(orbits) if val[1] == obj]
  if(len(direct_orbit) > 0):
    return [orbits[direct_orbit[0]][0]]
  else:
    return []

def count_orbits(orbits):
  direct_orbits = len(orbits)
  indirect_orbits = 0

  for orbit in orbits:
    check_orbit = True
    check = orbit[0]
    while (check_orbit == True):
      new_orbit = find_direct_orbit(check,orbits)
      if (len(new_orbit) > 0):
        check = new_orbit[0]
        indirect_orbits += 1
      else:
        check_orbit = False

  return direct_orbits+indirect_orbits

def find_indirect_orbits(obj,orbits):
  indirect_orbits = [x for x, val in enumerate(orbits) if val[0] == obj]
  if(len(indirect_orbits) > 0):
    result = []
    for io in indirect_orbits:
      result.append(orbits[io][1])
    return result
  else:
    return []

def crawl(begin, dest, orbits):
  transfer_map = [tuple(['YOU',-1])]
  check_orbits = [begin]
  distance = -1
  crawl_continue = True
  while (crawl_continue == True):
    new_orbits = []
    for orbit in check_orbits:
      new_orbits.extend(find_direct_orbit(orbit,orbits))
      new_orbits.extend(find_indirect_orbits(orbit,orbits))
    new_orbits = list(map(str,(set(new_orbits) - set(x[0] for x in transfer_map))))
    if (len(new_orbits) > 0):
      for orbit in new_orbits:
        existing_entry = [x for x, val in enumerate(transfer_map) if val[0] == orbit]
        if (len(existing_entry) > 0):
          existing_distance = transfer_map[existing_entry[0]][1]
          if (existing_distance > distance): 
            transfer_map.append(tuple([orbit,distance]))
        else:
          transfer_map.append(tuple([orbit,distance]))
    else:
      crawl_continue = False
    check_orbits = new_orbits[:]
    distance += 1
  dest_distance = [x[1] for x in transfer_map if x[0] == dest]
  return dest_distance[0]

#print(count_orbits(orbits))
print(crawl('YOU','SAN',orbits))