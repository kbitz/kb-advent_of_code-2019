import itertools

input_file = open("2_input.txt","r")
input_2 = input_file.readlines()
input_file.close()
input_2 = input_2[0].split(',')
input_2 = list(map(int, input_2))

def intcode(input_list,noun=None,verb=None):
  if noun is not None:
    input_list[1] = noun
  if verb is not None:
    input_list[2] = verb
  output = input_list
  i=0
  while (i < len(input_list)):
    if (i % 4 == 0) or (i == 0):
      if (input_list[i] == 1):
        output[input_list[i+3]] = input_list[input_list[i+1]] + input_list[input_list[i+2]]
        i += 1
      elif (input_list[i] == 2):
        output[input_list[i+3]] = input_list[input_list[i+1]] * input_list[input_list[i+2]]
        i += 1
      elif (input_list[i] == 99):
        break
      else:
        return "ERROR"
    else:
      i += 1
  output = output[0]
  return output

print("2.1 =",intcode(input_2[:],12,2))

i = 0
combos = []
while (i <= 99):
  combos.append(i)
  i += 1
combos = list(itertools.product(combos,combos))

for combo in combos:
  output = intcode(input_2[:],int(combo[0]),int(combo[1]))
  if (output == 19690720):
    print("2.2 =",100 * combo[0] + combo[1])
  else:
    continue