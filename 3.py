import numpy as np
import pandas as pd

file_input = open("3_input.txt","r")
input_3 = file_input.readlines()
file_input.close()

wire1 = input_3[0].strip('\n').split(',')
wire2 = input_3[1].strip('\n').split(',')

def wiremap(wire1,wire2):
    
  def gen_coord(wire):
    x=0
    y=0
    output = []
        
    for inst in wire:
      dx=0
      dy=0
      x1=0
      y1=0
      if (inst[:1] == 'U'):
        dy += int(inst[1:])
        y1 = y + dy
        while (y1 > y):
          y += 1
          output.append((x,y))
      elif (inst[:1] == 'D'):
        dy -= int(inst[1:])
        y1 = y + dy
        while (y1 < y):
          y -= 1
          output.append((x,y))
      elif (inst[:1] == 'R'):
        dx += int(inst[1:])
        x1 = x + dx
        while (x1 > x):
          x += 1
          output.append((x,y))
      elif (inst[:1] == 'L'):
        dx -= int(inst[1:])
        x1 = x + dx
        while (x1 < x):
          x -= 1
          output.append((x,y))

    return output
  
  wire1_coords = pd.DataFrame(gen_coord(wire1[:]),columns=['x','y'])
  wire2_coords = pd.DataFrame(gen_coord(wire2[:]),columns=['x','y'])
  
  wire1_coords['step'] = wire1_coords.index + 1
  wire2_coords['step'] = wire2_coords.index + 1

  matches = pd.merge(wire1_coords,wire2_coords,on=['x','y'],how='inner',suffixes=('_1','_2'))

  matches['manhattan'] = matches['x'].abs() + matches['y'].abs()
  matches['step_sum'] = matches['step_1'] + matches['step_2']

  
  return {'manhattan': matches['manhattan'].min(), 'steps': matches['step_sum'].min()}

print(wiremap(wire1[:],wire2[:]))