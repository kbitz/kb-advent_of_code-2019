import numpy as np
import pandas as pd

file_input = open("1_input.txt","r")
input_1 = file_input.readlines()
file_input.close()
input_1 = list(map(float, input_1))

fuel_total = 0

for module in input_1:
    fuel = np.floor(module / 3) - 2
    new_fuel = fuel
    while (new_fuel > 0):
        if (new_fuel < fuel):
            fuel += new_fuel
        new_fuel = np.floor(new_fuel / 3) - 2
    fuel_total += fuel

print(int(fuel_total))