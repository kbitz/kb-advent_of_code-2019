input_file = open("5_input.txt","r")
input_5 = input_file.readlines()
input_file.close()
input_5 = input_5[0].split(',')
input_5 = list(map(int, input_5))

def intcode(l: list, p: int):
   
  def interpret(s: int):
    s = str(s)
    r = list(s[::-1])
    i = len(r)
    while (i < 5):
      r.append('0')
      i += 1
    r[0] = r[1] + r[0]
    r.pop(1)
    return list(map(int, r))

  i = 0
  while (i < len(l)):
    ic = interpret(l[i])

    if(ic[0] == 1):
      o = 0
      o += l[l[i+1]] if ic[1] == 0 else l[i+1]
      o += l[l[i+2]] if ic[2] == 0 else l[i+2]
      if (ic[3] == 0):
        l[l[i+3]] = o
      else:
        l[i+3] = o
      i += 4

    elif(ic[0] == 2):
      o = 0
      o += l[l[i+1]] if ic[1] == 0 else l[i+1]
      o *= l[l[i+2]] if ic[2] == 0 else l[i+2]
      if (ic[3] == 0):
        l[l[i+3]] = o
      else:
        l[i+3] = o
      i += 4
    
    elif(ic[0] == 3):
      if (ic[1] == 0):
        l[l[i+1]] = p
      else:
        l[i+1] = p
      i += 2

    elif(ic[0] == 4):
      if (ic[1] == 0):
        print(l[l[i+1]])
      else:
        print(l[i+1])
      i += 2

    elif(ic[0] == 99):
      print('Program complete.')
      return 0
    
    elif(ic[0] == 5):
      p1 = l[l[i+1]] if ic[1] == 0 else l[i+1]
      if (p1 != 0):
        i = l[l[i+2]] if ic[2] == 0 else l[i+2]
      else:
        i += 3

    elif(ic[0] == 6):
      p1 = l[l[i+1]] if ic[1] == 0 else l[i+1]
      if (p1 == 0):
        i = l[l[i+2]] if ic[2] == 0 else l[i+2]
      else:
        i += 3

    elif(ic[0] == 7):
      p1 = l[l[i+1]] if ic[1] == 0 else l[i+1]
      p2 = l[l[i+2]] if ic[2] == 0 else l[i+2]
      if (p1 < p2):
        if (ic[3] == 0):
          l[l[i+3]] = 1
        else:
          l[i+1] = 1
      else:
        if (ic[3] == 0):
          l[l[i+3]] = 0
        else:
          l[i+1] = 0
      i += 4

    elif(ic[0] == 8):
      p1 = l[l[i+1]] if ic[1] == 0 else l[i+1]
      p2 = l[l[i+2]] if ic[2] == 0 else l[i+2]
      if (p1 == p2):
        if (ic[3] == 0):
          l[l[i+3]] = 1
        else:
          l[i+1] = 1
      else:
        if (ic[3] == 0):
          l[l[i+3]] = 0
        else:
          l[i+1] = 0
      i += 4
    
    else:
      print('ERROR')
      return 1

intcode(input_5[:], 1)
intcode(input_5[:], 5)