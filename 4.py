from collections import Counter

passwords = list(map(str, list(range(402328,864247 + 1))))
result1 = []
result2 = []

for pw in passwords:
  if list(pw) == sorted(pw):
    c = Counter(pw).values()
    for val in c:
      if (val >= 2):
        result1.append(pw)
        break

for pw in result1:
  c = Counter(pw).values()
  for val in c:
    if (val == 2):
      result2.append(pw)
      break

print(len(result1), len(result2))